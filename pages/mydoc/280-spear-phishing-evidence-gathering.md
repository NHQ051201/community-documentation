---
title: Warning on Possible Spear-Phishing Attacks
keywords: email templates, phishing, spearphishing, intermediaries, scamming, malware
last_updated: August 3, 2022
tags: [vulnerabilities_malware_templates, templates]
summary: "Template for writing to intermediaries to gather information on a possibly targeted phishing attack"
sidebar: mydoc_sidebar
permalink: 280-spear-phishing-evidence-gathering.html
folder: mydoc
conf: Public
ref: Spear-Phishing-Evidence-Gathering
lang: en
---


# Warning on Possible Spear-Phishing Attacks
## Template for writing to intermediaries to gather information on a possibly targeted phishing attack


### Body

Hi {{ intermediary name }},

We hope this email finds you well.

Our Access Now Helpline Team has noticed a worrying attack trend that has been targeting [[ group of users ]] lately. We are worried about the privacy and security of our users who you connected us with, and for this reason we are reaching out to inform you of these attacks. Besides warning you, we'd also like to ask if you've seen this type of attack in your network, and of course we'd like to provide our support and assistance if you need it.

This attack is a phishing attack targeting {{ group of users }}, and it is based on [[ vulnerability or feature the attack exploits ]].

Here’s how this attack works: {{ description of the attacks }}.

That's the basic summary. [[ insert any other details ]]

Please don't hesitate to reach out to us to report an ongoing or resolved incident of this type.

If you'd like to report an incident, it would be helpful if you could send us the following:

- To perform a full analysis of the email, we need a copy of the email. In this link you can find detailed instructions to export email messages as a .eml file for different email services and clients: https://mediatemple.net/community/products/dv/360021670392/how-to-export-emails-as-a-(.eml)-file. If you need any help with this, please let me know.
- Any other details that may help give us a better idea of how this attack happened.

Your input is highly appreciated, as we are trying to analyse all of these incidents so that we can better warn our users and provide them with recommendations to prevent these attacks.

Best regards,

Access Now Helpline Team


* * *


### Related Articles

- [Article #281: How to Recognize Spear-Phishing and What to Do](281-spear-phishing.html)
