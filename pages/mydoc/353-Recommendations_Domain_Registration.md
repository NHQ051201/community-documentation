---
title: Recommendations for Domain Registration and Disputes
keywords: domain registration, domain name, domain registrars, anonymous domain, website management
last_updated: April 4, 2019
tags: [infrastructure, articles]
summary: "A client would like to register a new domain and needs advice on how to choose a registrar, or has issues with the registrar and needs advice on how to address the dispute."
sidebar: mydoc_sidebar
permalink: 353-Recommendations_Domain_Registration.html
folder: mydoc
conf: Public
lang: en
---


# Recommendations for Domain Registration
## Advice on website domain registration and domain registration disputes

### Problem

1. A client needs to perform domain-related administrative tasks such as domain renewal and transfer, but their registrar is not responding.

2. A client is launching a new website and needs advice on domain registration.

Several problems might be associated with these needs:

- The client might be located in a country where registration of a civil society group/organization is not allowed by the government or risky if they choose to do so.
- The registrant information is recorded to an individual (e.g. incarcerated, high-risk).
- The domain registrar prohibits administrative requests such as domain renewal, update, or transfer made by colleagues of the owner of the domain.
- The client might be located in a country where electronic payment is restricted.
- The client's budget might be limited.


* * *


### Solution

#### Client's legal needs

Domain registrars have the authority to lock a domain. The policies they adhere to are based on ICANN's rules to protect the domain name from hijacking or fraud during transfer, renewal, or update.

A consent letter must be presented to the domain registrar to perform administrative tasks on behalf of the domain owner.

##### Domain renewal issues

If the domain has already expired and the owner is encountering a domain renewal issue with their registrar (e.g. refusal to renew, no notice of expiration served upon domain suspension), the standard process is to ask the client to submit a complaint through this [form](https://forms.icann.org/en/resources/compliance/complaints/renewal/form). Access Now Helpline can be consulted to flag the result to ICANN.
#### Domain registration

Below are the recommendations that we can provide to our clients who are launching a new website and need advice on domain registration.

1. Keep the domain name separated from the web hosting provider.

    Registering a domain with a different company than the hosting provider is advisable to avoid a potential lock if the domain name is tied to your web host, or a loss of the domain name (e.g. if a web hosting service got hacked, the client's domain name could be transferred elsewhere without their authorization).

2. Register the domain name with a registrar outside of your country.

    In repressive regimes, authorities can easily trace someone's identity through the registrar, and can pressure the registrar to make unfavorable decision/actions on a domain. This affects in particular individuals who have a high-risk profile or are incarcerated.
3. The registrant should be an entity rather than an individual.

    Having a domain name registered to an individual has a lot of risks involved, as the registrant is the only person who has the authorization to change ownership of or renew the domain name. If they are not present to perform administrative action to the domain, the team's entire operations may be stopped.

4. Choose a registrar that offers private domain registration.

    We recommend registrars like [Gandi.net](https://www.gandi.net) or [Hover](https://www.hover.com/whoisprivacy), that hide the name and email address of the registrant by default for free whenever possible (some top-level domains don't allow this option).

    If the client needs to buy a domain anonymously, they can do it on the privacy-friendly domain registrar [Njalla](https://njal.la/), which buys domains for their clients without asking for personal information. Njalla supports registration through Jabber+OTR or PGP-encrypted emails.

    For more companies that offer domain registration you may refer to [Article #88: Advice on Hosting](88-Advice_Hosting.html).

5. Online payment and auto-renewal terms

    If the client needs to keep their identity clearly separated from their domain, they should also use a prepaid credit card that is not connected to their own identity (if available in their country), and opt for domain auto-renewal, which can further protect them from loss or downtime of their domain, as well as from spam/phishing attacks based on domain renewal. In some countries where prepaid credit cards are not available, there might be the possibility to buy a gift card at a supermarket that allows for purchasing domain registration, or to use Bitcoin.

6. Check the Domain Transfer fees

    Check that domain transfer fees are low or for free (this also depends on what top-level domain the client is using and which registrar the domain is moved to). It is important to take note of such fees in future situations where the domain owner needs to change to another registrar in case the current registrar shows hostile behaviour.
    
7. Grace Period

    Some domain registrars strategically don't have a grace period, which is troublesome for registrants. A reminder can be set up or the registrar can send renewal notification, but experience shows that the registrar (or government) could try to hinder the process to stop renewal from happening in time, which can lead to losing a domain if a grace period is not granted.

* * *


### Comments

The cost of domain name registration and renewal often depends on the chosen top-level domain. If the client's budget is low, consider recommending them to explore the prices of different top-level domains before they register an expensive one.

* * *


### Related Articles

- [Article #88: Advice on Hosting](88-Advice_Hosting.html)
