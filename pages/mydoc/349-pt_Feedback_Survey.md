---
title: Portuguese - Feedback Survey Template
keywords: email templates, client feedback, case handling policy, followup
last_updated: August 12, 2021
tags: [helpline_procedures_templates, templates]
summary: "Email to be sent to Portuguese-speaking clients or requestors when closing a ticket"
sidebar: mydoc_sidebar
permalink: 349-pt_Feedback_Survey.html
folder: mydoc
conf: Public
ref: feedback-survey
lang: pt
---


# Feedback Survey Template [Portuguese]
## Email to be sent to Portuguese-speaking clients or requestors when closing a ticket

### Body

Olá {{ beneficiary name }},

Obrigado por entrar em contato com a Digital Security Helpline, administrada pela organização internacional de direitos humanos Access Now - https://accessnow.org.

Esta mensagem é para notificar sobre o fechamento de seu caso intitulado "{{ email subject }}".

A sua opinião é importante para nós. Se você deseja deixar comentarios sobre sua experiência com a Linha de Ajuda em Segurança Digital do Access Now, preencha a seguinte pesquisa:

https://form.accessnow.org/index.php/139723?lang=pt-BR&139723X20X841={{ ticket id }}

Seu número de caso é: {{ ticket id }}

Se você tiver outras dúvidas ou preocupações, informe-nos e teremos prazer em ajudar.

Obrigado,

{{ incident handler name }}
