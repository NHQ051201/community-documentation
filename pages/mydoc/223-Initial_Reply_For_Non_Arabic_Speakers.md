---
title: Initial Reply - For Non-Arabic Speakers
keywords: email templates, initial reply, case handling policy
last_updated: August 12, 2021
tags: [helpline_procedures_templates, templates]
summary: "First response, Email to Client for Non-Arabic speakers"
sidebar: mydoc_sidebar
permalink: 223-Initial_Reply_For_Non_Arabic_Speakers.html
folder: mydoc
conf: Public
lang: ar
---

أهلا بك،

أنا
{{ incident handler name }}
عضو من فريق مساعدي الأمان الرقمي لأكساس ناو.
مساعدو-الأمان-الرقمي
https://www.accessnow.org/

لقد تلقينا البريد الإلكتروني الذي بعثته
{{ email subject }}
مع الأسف لن نستطيع التواصل معك بالعربية
في صورة ما إذا تتطلب الوضعية التدخل السريع، الرجاء الإجابة و إضافة كلمة
“URGENT”
إلى موضوع البريد الإلكتروني.

إن كنتم قادرين على التواصل باللغة

[[ EN ]] الإنجليزية

[[ FR ]] أو الفرنسية

[[ DE ]] أو الألمانية

[[ ES ]] أو الإسبانية

[[ IT ]] أو الإيطالية

[[ PT ]] أو البرتغالية

[[ RU ]] أو الروسية

سنكون قادرين على الإجابة على أسئلتكم مباشرة الآن

سيكون زملاؤنا الذين يتكلمون العربية على الخط بعد سويعات

شكراً .

{{ incident handler name }}


* * *


### Related Articles

- [Article #154: FAQ - Initial Reply](154-FAQ-Initial_Reply.html)
