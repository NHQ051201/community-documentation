---
title: Protocol for a Client We Suspect Is Paranoid
keywords: helpline procedures, paranoia, psychological security, client in distress, initial reply
last_updated: April 29, 2019
tags: [helpline_procedures, articles]
summary: "A client is talking about threats that don't seem realistic or we suspect they are paranoid for other reasons and don't know how to react."
sidebar: mydoc_sidebar
permalink: 356-paranoia_protocol.html
folder: mydoc
conf: Public
ref: paranoia_protocol
lang: en
---

# Protocol for a Client We Suspect Is Paranoid
## What to do when we suspect a client might be paranoid

### Problem

Some clients may exaggerate or misunderstand or have a false belief, worrying about a potential threat that is not realistic.  This may be a case of “paranoia.”

We call "paranoia" a concern or suspicion with no rational justification. Typical is a firmly held belief, which is false, that someone is targeting them for harm, without evidence and without a logical reason.


* * *


### Solution

1. If you suspect someone is in a state of paranoia, start by asking yourself the right questions:
    - Who is causing/threatening harm?  What type of harm?  What is their motive for harming the client? Is this logical, and does the severity and type of reported or threatened harm match the alleged motive?  For example, if a client believes someone is trying to harm them because they have information, does the type of harm alleged compare to the type of information? **If the threat is out of proportion (i.e. larger, more severe) to the motive, it may be paranoia**.
    - Is it possible that the client has a legitimate concern?
    - Can you establish trust with the client? Does the client see you as someone who can help them or do they see you as someone who is part of the threat/conspiracy? Is the client aggressive with you? Does the client refuse or reject any/all help you offer?

2. Psychological tips - how to behave
    - Your goal is to establish trust so you can assess whether the threat is real.  To do this, express empathy as often as you can (e.g., “It sounds like you are having a tough time.,” “I can tell how scared you are,” “I am sorry this is happening to you.”).
    - Do not challenge the client’s false beliefs. Accept that they believe they are telling the truth. Ask open-ended questions (e.g., “Tell me about your concern,” “How is the person threatening you?”  “What do they want from you?” “Why might they want to harm you?”), rather than “yes” or “no” questions when you are first gathering information and establishing trust.
    - If they are aggressive or rejecting of help, try to express empathy (e.g., “I am sorry I am not helping the way you want. Do you have any suggestions for ways I can help you?” - “In the best circumstances, what would you like to see happen?” - “I am sorry you are so scared. I can understand why.”)
    - Paranoia is a psychological condition where the person is not able to be rational. Please try not to take it personally if they are aggressive or rejecting of your help.

You can find a template for initial replies to clients you suspect are paranoid in [Article #357: Initial Reply for Clients You Suspect Are Paranoid](357-paranoia_template.html).

* * *


### Comments



* * *

### Related Articles

- [Article #357: Initial Reply for Clients You Suspect Are Paranoid](357-paranoia_template.html)
