---
title: Post-DSC Outreach Template - Spanish
keywords: email templates, outreach, events, DSC, digital security clinics
last_updated: August 12, 2021
tags: [helpline_procedures_templates, templates]
summary: "Email template for outreach in Spanish - addressed at members of civil society we met during a Digital Security Clinic or an event"
sidebar: mydoc_sidebar
permalink: 152-Post-DSC_Outreach_Template_es.html
folder: mydoc
conf: Public
ref: Post-DSC_Outreach_Template
lang: es
---


# Post-DSC Outreach Template - Spanish
## Email template for outreach - addressed at members of civil society we met during a Digital Security Clinic or an event

### Body

Estimado/a {{ beneficiary name }},

Le escribe {{ incident handler name }} de la Línea de Ayuda en Seguridad Digital de Access Now. Fue un gusto conversar durante {{ event name }}.

Solamente estoy enviando este correo para recordarle que nuestra línea de ayuda se encuentra disponible 24/7 todos los días del año para cualquier consejo y asistencia en temas de seguridad digital. Puede encontrar más información sobre la línea de ayuda en https://www.accessnow.org/help-es/ y siempre puede contactarnos al correo electrónico help@accessnow.org.

Adicionalmente estoy adjuntando nuestra llave PGP. Si aún no tiene configurado cifrado en su correo electrónico, gustosamente podemos colaborar; de forma tal que creemos un canal de comunicación seguro en caso de necesitarlo en un futuro.

Al igual que antes, quedamos a total disposición para cualquier pregunta o asistencia.

Gracias,

{{ incident handler name }}
