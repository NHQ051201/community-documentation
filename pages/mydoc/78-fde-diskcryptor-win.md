---
title: FDE with DiskCryptor on Windows 
keywords: FDE, full-disk encryption, device security, mobile security, Windows, DiskCryptor
last_updated: October 01, 2018
tags: [devices_data_security, articles]
summary: "A client has an unencrypted machine and their version of Windows does not support Bitlocker."
sidebar: mydoc_sidebar
permalink: 78-fde-diskcryptor-win.html
folder: mydoc
conf: Public
lang: en
---


# FDE with DiskCryptor on Windows
## How to encrypt an older Windows computer with DiskCryptor

### Problem

The client laptop might show a Windows automatic repair screen after attempting to encrypt with DiskCryptor

This might be because DiskCryptor only works if the hard drive is using Master Boot Record (MBR) as the partitioning scheme. The machine will not boot if you encrypt a Windows machine that uses GPT.


* * *


### Solution

**Please note that Bitlocker should be the recommendation for encrypting Windows machines, as DiskCryptor is currently unmaintained and its latest version dates back to 2014. If the client's version does not support Bitlocker, we can recommend the use of [VeraCrypt](https://www.veracrypt.fr/en/Home.html) to create an encrypted folder**.

1. Consider creating a [DiskCryptor liveCD](https://diskcryptor.net/wiki/LiveCD) to be able to decrypt the hard drive in case there is a major issue.

2. Check what partitioning scheme the machine uses:

    Control Panel / Administrative Tools / Computer Management / DiskManagement / select drive / Properties / Volumes - it will say either GPT or MBR.

    If the machine is using MBR, then you can proceed with the DiskCryptor download, installation and encryption. [This video tutorial might he helpful](https://www.youtube.com/watch?v=HaQEzA2ye4U).

3. If the machine is using GPT, you need to find a way to change it to MBR. Some considerations:

    - MBR *only* works with Legacy boot. Check if this option is supported in the BIOS before making any change. 
    - MBR limits the amount of partitions to 4. If the disk has more than 4 partitions, you would have to delete or merge partitions. If not possible, then the transformation to MBR cannot be done.
    - MBR maximum partition size is 2 TB.

    If all the above considerations are fulfilled, then you can proceed with the transformation. Some options can be found here: [AOEMI Partition Helper Full version](http://www.sevenforums.com/tutorials/26203-convert-gpt-disk-mbr-disk.html)

4. Once the laptop disk is set to use MBR, you can proceed with the encryption following [this video tutorial](https://www.youtube.com/watch?v=HaQEzA2ye4U)


* * *


### Comments

Always make a full backup of the drive information before attempting to encrypt the system!


* * *

### Related Articles

- [Article #166: FAQ - Full-Disk Encryption (FDE)](166-Full-Disk_Encryption.html)