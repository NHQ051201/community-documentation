---
title: Escalations To Platforms With No Escalation Channel
keywords: Account Recovery, Account security, Data leaks, Harassment
last_updated: July 14, 2022
tags: [account_recovery, articles]
summary: "How to handle a case that requires escalation to an online platform, host provider or registrar where the Helpline has not already established an escalation channel."
sidebar: mydoc_sidebar
permalink: 428-Escalations_To_Platforms_With_No_Escalation_Channel.html
folder: mydoc
conf: Public
ref: Escalations_To_Platforms_With_No_Escalation_Channel
lang: en
---


# Escalations To Platforms With No Escalation Channel
## How to contact a platform with no existing escalation channel

### Problem

In order to swiftly intervene to resolve security issues for its constituency, the Helpline has been working to establish direct communication channels with several social online platforms, email providers and hosting providers. However, we often have to communicate with platforms with whom we have no established escalation channel in order to respond to cases that are related to online accounts or online content. This article will guide the handler on how to handle such requests. 

* * *


### Solution

#### Mandate Check:

Even if the Helpline has no escalation channel with the platfom in question, the case will be considred within mandate if it fullfills our [Mandate checklist](https://procedures.accessnow.org/Mandate_Checklist.html). 

#### Reminder on Sharing Client's Information to 3rd parties:

Please remember that information shared by beneficiaries on cases is considered RESTRICTED:HELPLINE as per our [information classification policy](https://procedures.accessnow.org/Information_classification.html). However, in our [terms of service](https://www.accessnow.org/helpline-terms-of-service/) we reserve the right to share information with 3rd parties (including external service providers in this case) when necessary to solve issues on which the client requests assistance. Please limit the amount of information shared to the minimum necessary to successfully resolve the case. 

#### How to dig for official public channels:

##### Official email addresses of the support or security teams:

In order to escalate the case, we should find a communication channel that the support or security teams of the platform will receive requests on. This channel could be an email alias such as support@, contact@, or security@, and may be found under pages like 'Contacts', 'Support', 'Security', or 'Report a problem' on the official website of the platform. Browse the website for pages with similar titles. Usually they are linked in the footer of the website. 

If the company follows the guidelines set on [RFC 2142](https://datatracker.ietf.org/doc/html/rfc2142), they will normally respond to the abuse@, NOC@ or security@ mailboxes.

##### Social media channels:

Some companies also have a report intake mechanism through their social media account, commonly on Twitter. Contacting the platform through their Twitter or other social media account can also be tried as an alternative. Make sure to verify that the account really belongs to them: some good indicators of this are if the account is verified, if the account has appropriate posting and content, if the account has a good number of followers, and if their official website links to the account. 


###### The Whois Record:

Registrars and host providers usually provide email addresses to report abuses on the Whois page. This would be especially useful if the cases require taking down a content of a website. Search the Whois of its domain or the resolved IP to find these email addresses and contact the support team using them. Here you can find more information on [how to obtain whois records](https://git.accessnow.org/access-now-helpline/faq/-/blob/Doc-Updates/Web_Vulnerabilities/377-Website_Vulnerability_Assessment.md#a-whois). 

###### Contact Forms:

In many cases, platforms provide a support form in addition to or instead of email addresses. Filling out these forms will usually create a ticket in their Support team's ticketing systems, and so this method can be a quicker communication channel. 

###### The Escalation Process:

In case the above solutions do not work, please use our escalation process to escalate this case to the Service Platform Analyst, who can work with other internal and external parties to find escalation channels for your case. 

The following are templates you may use as a base to work from:
- [Disable Malicious Domain](https://git.accessnow.org/access-now-helpline/faq/-/blob/Doc-Updates/Templates/259-Disable_Malicious_Server_registrar.md)
- [Disable Malicious Content or IP](https://git.accessnow.org/access-now-helpline/faq/-/blob/Doc-Updates/Templates/260-Disable_Malicious_Server_hosting_provider.md)
- [Disable Malicious Domain in Russian](https://git.accessnow.org/access-now-helpline/faq/-/blob/Doc-Updates/Templates/313-Disable_Malicious_Server_registrar_ru.md)
- [Disable Malicious Content or IP in Russian](https://git.accessnow.org/access-now-helpline/faq/-/blob/Doc-Updates/Templates/312-Disable_Malicious_Server_hosting_provider_ru.md)
- [Report Fake Domain](https://git.accessnow.org/access-now-helpline/faq/-/blob/Doc-Updates/Templates/343-Report_Domain_Impersonation_Cloning.md)
- [General Email template to platform that we do not have an escalation process](/link_to_be_added)


* * *


### Comments



* * *

### Related Articles

- [Article #23: FAQ - Account Recovery and Deactivation](23-FAQ-Account_Recovery_and_Deactivation.html)
